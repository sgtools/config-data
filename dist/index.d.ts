declare module '@sgtools/config' {
    interface Config {
        setDataPath(path: string): void;
        getDataPath(): string;
    }
}
export {};
