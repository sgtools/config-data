"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const os = require("os");
const path = require("path");
const application_1 = require("@sgtools/application");
const config_1 = require("@sgtools/config");
config_1.Config.prototype.setDataPath = (path) => {
    config_1.config.setString('dataPath', path);
};
config_1.Config.prototype.getDataPath = () => {
    let dPath = config_1.config.getString('dataPath');
    if (dPath === null)
        dPath = path.join(os.homedir(), '.' + application_1.app.name + '_data');
    return dPath;
};
