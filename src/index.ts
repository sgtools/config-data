import * as os from 'os';
import * as path from 'path';

import { app } from '@sgtools/application';

import { Config, config } from '@sgtools/config';

declare module '@sgtools/config'
{
    interface Config
    {
        setDataPath(path: string): void;
        getDataPath(): string;
    }
}

Config.prototype.setDataPath = (path: string) =>
{
    config.setString('dataPath', path);
}

Config.prototype.getDataPath = () =>
{
    let dPath = config.getString('dataPath');

    if (dPath === null) dPath = path.join(os.homedir(), '.' + app.name + '_data')

    return dPath;
}